# On hiatus until I get zwm into a more final state and wont be making massive structural changes to it. All of these can still be implemented manually if you would like as far as I know, the logic is very similar if not identical
# zwm patches
A collection of patches I'm gradually making for my window manager for functionality I appreciate but don't want in the main branch \
Some patches require manual editing to actually be implemented if you are applying several at once. \
Currently, none of these will apply correctly and need to be remade from scratch after a drastic overhaul of some source code. The logic should be the same and can be implemented with technical know-how, but the patches themselves cannot be applied normally for the time being.

# Applying Patches
Simply copy them into the source code directory on your machine and run 
```bash
git apply <patch-file-name>
```

# Patches
## Autostart
Reads a bash script at a location specified and executes it on start
## Sloppy Mouse
Lets you use the mouse to focus the window - for the mouse enthusiasts
## Swap on Move
Switches the focused workspace when you move windows, purely personal preference if you like this or not, so I made it optional
## Bar Offset
Creates a gap at the top of the screen (specified in your `config.h`) to leave space for any bars your would like to add.
## Borders
Adds borders, change colors and size in `config.h`.

# Making patches
If anyone wants to make some patches and send them my way but isn't sure how, simply making you changes in corresponding files on a clean copy of zwm, typically `zwm.c` and `config.def.h` and run
```bash
git diff >> <patch-file-name>
```
Then just make a fork of this repo, toss that file in it, and send me a pull request.
